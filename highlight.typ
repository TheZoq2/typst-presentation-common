#import "util.typ" : push_line_number
#import "pins.typ" : pinned_range, pin_up, pin_down

#let rgbf(r, g, b) = rgb(int(r*255), int(g*255), int(b*255))


#let darkTheme = (
  keywordColor: rgbf(0.5, 1.0, 1.0),
  typeColor: rgbf(0.67, 0.81, 0.17),
  conditionalColor: rgbf(0.94, 0.63, 0.94),
  builtinColor: rgbf(1.0, 0.37, 0.68),
  operatorColor: rgbf(0.88, 0.65, 0.10),
  typeOperatorColor: rgbf(0.88, 0.65, 0.10),
  stringColor: rgbf(0.55, 0.71, 0.33),
  boolLiteralColor: rgbf(0.88, 0.65, 0.10),
  selfColor: rgbf(0.88, 0.34, 0.62),
  literalColor: rgbf(0.88, 0.34, 0.62),
  attributeColor: rgbf(1.0, 0.55, 1.0),
  namespacePartColor: rgb("#ff3537"),
  paramColor: rgb("#df875f"),
  constantColor: rgbf(0.88, 0.34, 0.62),
  commentGreen: rgbf(0, 0.8, 0.5),
  bgColor: rgbf(0,0,0),
  errorRed: rgbf(1.0,0.25,0.25),
  errorBlue: rgbf(0,0.55,1.0),
  errorOrange: rgbf(0.72,0.62,0.03),
  okGreen: green.lighten(50%),
  OliveGreen: rgbf(0,0.6,0),
)


#let lightTheme = (
  keywordColor: rgb("#4271ae"),
  typeColor: rgb("#006700"),
  conditionalColor: rgb("#8959a8"),
  builtinColor: rgb("#d7005f"),
  operatorColor: rgb("#3e999f"),
  typeOperatorColor: black,
  stringColor: rgb("#5f00ff"),
  boolLiteralColor: rgb("#008700"),
  selfColor: rgb("#d7005f"),
  literalColor: rgb("#d75f00"),
  attributeColor: rgb("#4271ae"),
  namespacePartColor: rgb("#e00505"),
  paramColor: rgb("#870049"),
  commentGreen: rgb("#008700"),
  constantColor: rgb("#d7005f"),
  bgColor: rgbf(1,1,1),
  errorRed: rgb("#ad251b"),
  errorBlue: rgb("#003f8e"),
  errorOrange: rgb("#b69700"),
  okGreen: green.darken(50%),
  OliveGreen: rgb("#417116"),
)

// Split the string into an array where all occurrences of symbols in `symbols`
// have are their own element. Symbols cannot be regex
#let tokenize_symbols(symbols, s) = {
  if symbols.len() == 0 {
    (s,)
  } else {
    // I'm not sure why we get empty strings when tokenizing " ", but we do
    // so the last filter is there to work around that
    s
      .split(symbols.at(0))
      .map(rest => tokenize_symbols(symbols.slice(1), rest))
      .intersperse(symbols.at(0))
      .flatten()
      .filter(c => c != "")
  }
}

#let is_whitespace(s) = {
  s == " " or s == "\n" or s == "\t"
}

#let kw2regex(keywords) = {
  regex("\b(" + keywords.join("|") + ")\b")
}


#let symbols2regex(symbols) = {
  regex("(" + symbols.join("|") + ")")
}

#let highlights(use_light_theme: false, run_parsers: true, doc) = [
  // #show raw: set block(spacing: 0.7em)

  #let theme = if use_light_theme {
    lightTheme
  } else {
    darkTheme
  }

  #let keywordColor = theme.keywordColor
  #let typeColor = theme.typeColor
  #let conditionalColor = theme.conditionalColor
  #let builtinColor = theme.builtinColor
  #let operatorColor = theme.operatorColor
  #let typeOperatorColor = theme.typeOperatorColor
  #let stringColor = theme.stringColor
  #let boolLiteralColor = theme.boolLiteralColor
  #let selfColor = theme.selfColor
  #let literalColor = theme.literalColor
  #let attributeColor = theme.attributeColor
  #let namespacePartColor = theme.namespacePartColor
  #let commentGreen = theme.commentGreen
  #let constantColor = theme.constantColor
  #let paramColor = theme.paramColor
  #let bgColor = theme.bgColor
  #let errorRed = theme.errorRed
  #let errorBlue = theme.errorBlue
  #let errorOrange = theme.errorOrange
  #let okGreen = theme.okGreen
  #let OliveGreen = theme.OliveGreen

  #let collect_metadata(it, number) = {
    show regex("%[^%]+%") : w => {
      let ref = w.text.find(regex("<.*>"))
      if ref != none {
        let name = ref.replace("<", "").replace(">", "")
        push_line_number(number, name)
      }
    }
    it
  }

  #let gather_metadata(raw_block) = {
    // Allow specifying metadata on the first line, i.e. after the language.
    // After this we need to generate a new block with that line stripped
    if raw_block.lines.at(0).text.starts-with(",") {
      let metas = raw_block
        .lines
        .at(0)
        .text.split(",")
        .map(meta => meta.trim())

      (
        raw_block.lines.slice(1).map(line => line.text).join("\n"),
        (
          lines: metas.contains("lines"),
          frame: metas.contains("frames"),
          fill_width: metas.contains("fill_width")
        )
      )
    } else {
      (raw_block.text, (lines: false, fill_width: false, frame: false))
    }
  }

  #let style_lang(styler, preprocessor: none) = it => {
    let is_block = it.block;
    let (t, metadata) = gather_metadata(it)
    let t = if preprocessor != none and run_parsers {
      preprocessor(t)
    } else {
      t
    }

    show raw.line: it => {
      let line_content = styler(collect_metadata(it.body, it.number), it.number)

      let number_width = calc.log(it.count + 1, base: 10)
      let line_number = if metadata.lines {
        let missing_digits = calc.ceil(number_width) - calc.ceil(calc.log(it.number + 1, base: 10))
        text(fill: gray, {
          let padding = range(0, missing_digits).map(_ => " ").join()
          padding + str(it.number)
        })
        h(0.7em)
      } else {
        none 
      }

      line_number
      line_content
    }

    show raw: it => text(size: 1.0em, it)
    let it = raw(
      block: it.block,
      align: it.align,
      tab-size: it.tab-size,
      t
    )

    if is_block and not metadata.lines {
      // rect(
      //   stroke: none,
      //   fill: rgb(97%, 97%, 97%),
      //   width: if metadata.fill_width {97%} else {auto},
      //   if metadata.fill_width {
      //     box(width: 100%, {set align(left); it})
      //   } else {
      //     it
      //   }
      // )
      if metadata.frame {
        rect(
          stroke: (left: (paint: gray.lighten(20%), thickness: 1pt)),
          inset: (top: 0.3em, bottom: 0.3em, left: 0.7em),
          if metadata.fill_width {
            box(width: 100%, {set align(left); it})
          } else {
            it
          }
        )
      } else {
        it
      }
    } else {
      if metadata.fill_width {
        box(width: 100%, {set align(left); it})
      } else {
        it
      }
    }
  };

  #let spade_preprocessor = text => {
      let tokens = tokenize_symbols(("$(", "(", ")", ":", ",", "\n", " "), text)

      let trim_whitespace(tokens) = {
        let (arg, ..tokens) = tokens
        let leading_ws = ""
        while is_whitespace(arg) {
          leading_ws = leading_ws + arg
          (arg, ..tokens) = tokens
        }
        (leading_ws, (arg, ..tokens))
      }

      let parse_value(tokens) = {
        let paren_depth = 0
        let out = ""
        let result = none
        while result == none {
          let (first, ..next_tokens) = tokens

          if (first == "," or first == ")") and paren_depth == 0 {
            result = (out, (first, ..next_tokens))
            break
          } else if first == ")" {
            if paren_depth < 0 {
              panic("Missmatched parens before " + tokens.join(""))
            }
            paren_depth = paren_depth - 1
          } else if first == ")" {
            paren_depth = paren_depth + 1
          }
          out = out + first
          tokens = next_tokens
        }
        result
      }

      let parse_argument(tokens) = {
        let (leading_ws, (arg, ..tokens))  = trim_whitespace(tokens)

        // Mark this as a paremeter
        let out = leading_ws + "#param{" + arg + "}"

        let (mid_ws, tokens) = trim_whitespace(tokens)
        let (first, ..tokens) = tokens

        // If this was a shorthand named argument, we are done and can continue. Otherwise
        // we need to parse the body
        if first == "," {
          (out + mid_ws + ",", tokens)
        }
        else if first == ":" {
          let (value, tokens) = parse_value(tokens)
          (out + mid_ws + ":" + value, tokens)
        } else if first == ")" {
          (out + mid_ws, (first, ..tokens))
        } else {
          panic("Got unexpected token after '" + arg + "' which is a parameter. Got '" + first + "' expected , : or ). Before: '" + tokens.slice(5).join(""))
        }
      }

      let parse_arg_list(tokens) = {
        let (first, ..tokens) = tokens

        // Trailing commas are allowed. We'll allow more than one because it is easier
        if first == "," {
          let (out, tokens) = parse_arg_list(tokens)
          ("," + out, tokens)
        } else if first == ")" {
          (")", tokens)
        } else if is_whitespace(first) {
          let (ws, tokens) = trim_whitespace(tokens)
          let (rest, tokens) = parse_arg_list(tokens)
          (first + ws + rest, tokens)
        } else {
          let (first_arg, tokens) = parse_argument((first, ..tokens))
          let (ws, tokens) = trim_whitespace(tokens)
          let (remainder, tokens) = parse_arg_list(tokens)
          (first_arg + ws + remainder, tokens)
        }
      }

      let parse_top(tokens) = {
        let out = ""
        while tokens.len() != 0 {
          let (first, ..rest) = tokens
          if first == "$(" {
            let (arg_list, rest) = parse_arg_list(rest)
            out = out + "$(" + arg_list
            tokens = rest
          } else {
            out = out + first
            tokens = rest
          }
        }
        out
      }

      parse_top(tokens)
    }


  #show raw : it => {
    let pin_re = "\$pinned\(([A-z_0-9]+),\s?(.*)\)\$"
    show regex(pin_re): w => {
      let cap = w.text.match(regex(pin_re))
      cap.captures.at(1)
    }
    let pin_down_re = "\$pin_down\(([A-z_0-9]+)\)\$"
    show regex(pin_down_re): w => {}
    let pin_up_re = "\$pin_up\(([A-z_0-9]+)\)\$"
    show regex(pin_up_re): w => {}
    it
  }



  #show raw.where(lang: "spade"): style_lang(
    preprocessor: spade_preprocessor,
    (line, number) => {
      show kw2regex(
        (
          "entity",
          "fn",
          "reg",
          "let",
          "reset",
          "inst",
          "enum",
          "decl",
          "struct",
          "use",
          "as",
          "mod",
          "stage",
          "pipeline",
          "assert",
          "port",
          "set",
          "impl",
          "for",
          "where"
        )
      ) : keyword => text( weight:"bold", fill: keywordColor, keyword)

      show kw2regex(
        ("int","uint","bool","Option","clock")
      ) : w => text(fill: typeColor, w)

      show kw2regex(
        ("match", "if", "else")
      ) : w => text(weight: "bold", fill: conditionalColor, w)

      show kw2regex(("Some", "None")) : w => text(weight: "bold", fill: builtinColor, w)

      show symbols2regex(
        ("=", "-", "<", ">", "\+", "\|", ">>", "&", "\*")
      ) : w => text(weight: "bold", fill: operatorColor, w)

      show kw2regex(
        ("true", "false")
      ) : w => text(fill: boolLiteralColor, w)

      show kw2regex(
        ("&", "mut")
      ) : w => text(fill: typeOperatorColor, w)


      show kw2regex(
        ("self",)
      ) : w => text(fill: selfColor, w)

      show regex("'\w*") : w => emph(text(fill: attributeColor, w))
      show regex("stage\(\w*\)") : w => {
        let inner = w.text.replace("stage(", "").replace(")", "")

        [stage(#emph(text(fill: attributeColor, inner)))]
      }

      show regex("//.*") : w => text(fill: commentGreen, w)

      show regex("#\[[^\]]*\]") : w => text(fill: attributeColor, w)

      show regex("\b\d+\b") : w => text(fill: literalColor, w)
      show regex("\b0x[0-9a-fA-F_]+\b") : w => text(fill: literalColor, w)
      show regex("\b0b[01]+\b") : w => text(fill: literalColor, w)

      show(regex("#param\{[^\}]*\}")): w => {
        text(fill: paramColor, w.text.replace("#param{", "").replace("}", ""))
      }

      show regex("([A-z_0-9]+::)+[A-z_0-9]+"): w => {
        let parts = w.text.split("::");
        parts
          .slice(0, -1)
          .map(part => text(fill: namespacePartColor, weight: "bold", part))
          .join("::")
        [::]
        parts.at(-1)
      }

      let pinned_re = "\$pinned\(([A-z_0-9]+),\s?(.*)\)\$"
      show regex(pinned_re): w => {
        let cap = w.text.match(regex(pinned_re))
        pinned_range(cap.captures.at(1), cap.captures.at(0))
      }


      let pin_up_re = "\$pin_up\(([A-z_0-9]+)\)\$"
      show regex(pin_up_re): w => {
        let cap = w.text.match(regex(pin_up_re))
        pin_up(cap.captures.at(0))
      }
      let pin_down_re = "\$pin_down\(([A-z_0-9]+)\)\$"
      show regex(pin_down_re): w => {
        let cap = w.text.match(regex(pin_down_re))
        pin_down(cap.captures.at(0))
      }

      line
    }
  )

  #show raw.where(lang: "rustp"): style_lang((it, _number) => {
    show kw2regex(
      (
        "entity",
        "pub",
        "fn",
        "let",
        "enum",
        "struct",
        "use",
        "as",
        "mod",
        "assert",
        "set",
        "impl",
        "trait"
      )
    ) : keyword => text( weight:"bold", fill: keywordColor, keyword)

    show kw2regex(
      ("int","bool","Option","clock")
    ) : w => text(fill: typeColor, w)

    show kw2regex(
      ("match", "if", "else")
    ) : w => text(weight: "bold", fill: conditionalColor, w)

    show kw2regex(("Some", "None")) : w => text(weight: "bold", fill: builtinColor, w)

    show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&")
    ) : w => text(weight: "bold", fill: operatorColor, w)

    show kw2regex(
      ("true", "false")
    ) : w => text(fill: boolLiteralColor, w)

    show kw2regex(
      ("&", "mut")
    ) : w => text(fill: typeOperatorColor, w)


    show kw2regex(
      ("self",)
    ) : w => text(fill: selfColor, w)

    show regex("//.*") : w => text(fill: commentGreen, w)

    show regex("#\[[^\]]*\]") : w => text(fill: attributeColor, w)

    // morecomment=[l]{//}, % l is for line comment
    // morecomment=[s]{/*}{*/}, % s is for start and end delimiter
    // commentstyle=\color{commentGreen},
    // morestring=[b]", % defines that strings are enclosed in double quotes
    // escapechar=\%,
    it
  })

  #show raw.where(lang: "cppp"): style_lang((it, _number) => {
    show kw2regex(
      (
        "template",
        "typename",
        "using"
      )
    ) : keyword => text( weight:"bold", fill: keywordColor, keyword)

    show kw2regex(
      ("optional",)
    ) : w => text(fill: typeColor, w)

    show kw2regex(
      ("case", "of")
    ) : w => text(weight: "bold", fill: conditionalColor, w)

    show kw2regex(("void", )) : w => text(weight: "bold", fill: builtinColor, w)

    show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&")
    ) : w => text(weight: "bold", fill: operatorColor, w)

    show regex("//.*") : w => text(fill: commentGreen, w)

    it
  })

  #show raw.where(lang: "haskellp"): style_lang((it, _number) => {
    show kw2regex(
      (
        "data",
      )
    ) : keyword => text( weight:"bold", fill: keywordColor, keyword)

    show kw2regex(
      ("Maybe",)
    ) : w => text(fill: typeColor, w)

    show kw2regex(
      ("case", "of")
    ) : w => text(weight: "bold", fill: conditionalColor, w)

    show kw2regex(("Nothing", "Just")) : w => text(weight: "bold", fill: builtinColor, w)

    show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&")
    ) : w => text(weight: "bold", fill: operatorColor, w)

    show regex("--.*") : w => text(fill: commentGreen, w)

    // morecomment=[l]{//}, % l is for line comment
    // morecomment=[s]{/*}{*/}, % s is for start and end delimiter
    // commentstyle=\color{commentGreen},
    // morestring=[b]", % defines that strings are enclosed in double quotes
    // escapechar=\%,
    it
  })

  #show raw.where(lang: "wal_analysis"): style_lang((it, _number) => {
    show regex("ANALYSIS") : w => text(fill: commentGreen, w)
    show regex("INFO") : w => text(fill: keywordColor, w)

    // morecomment=[l]{//}, % l is for line comment
    // morecomment=[s]{/*}{*/}, % s is for start and end delimiter
    // commentstyle=\color{commentGreen},
    // morestring=[b]", % defines that strings are enclosed in double quotes
    // escapechar=\%,
    it
  })

  #show raw.where(lang: "directory"): style_lang((it, _number) => [
    #show symbols2regex(("├", "\|", "└", "─", "│")) : w => text(fill: rgbf(0.5, 0.5, 0.5), w)
    #show symbols2regex(("", )) : w => text(fill: boolLiteralColor, w)
    #show symbols2regex(("%spade%", )): w => {
      box(image("./fig/spadefish.svg", height: 0.8em))
    }
    #it
  ])


  #show raw.where(lang: "wal"): style_lang((it, _number) => {
    show kw2regex((
      "alias",
      "seta",
      "list",
      "when",
      "in-group",
      "in-groups",
      "groups",
      "print",
      "reval",
      "resolve-group",
      "step",
      "load",
      "find",
      "append",
      "while",
      "geta/default",
      "for/listextern",
      "call",
      "set",
      "do",
      "-",
      "min",
      "max",
      "average",
      "defun",
      "require",
      "printf",
      "count",
      "fdiv",
      "unload",
      "let",
      "cond",
      "lambda",
      "inc",
      "unalias",
      "get",
      "array",
      "mapa",
      "length",
      "map",
      "sum",
      "slice",
      "resolve-scope",
      "in-scope",
      "all-scopes",
      "timeframe",
      "sample-at",
      "defmacro",
      "macroexpand",
      "geta",
      "default",
      "in-spade-structs",
      "in-spade-struct",
      "spade-struct",
      "spade-structquasiquote",
      "spade/translate",
      "unquote",
      "quote",
      "fold",
      "rest",
      "fn",
      "reverse",
      "in",
      "define"
    )): w => text(fill: keywordColor, weight: "bold", w)

    // These don't work for some reason
    show symbols2regex(("@",)) : w => text(fill: rgbf(0.7, 0.0, 0.0), w)
    show symbols2regex(("#",)) : w => text(fill: rgbf(0.7, 0.0, 0.4), w)
    show kw2regex((
      ";",
      "INDEX",
      "TS",
      "CG",
      "CS",
      "stage"
    )) : w => text(fill: rgbf(0.8, 0.3, 0.0), weight: "bold", w)
    show kw2regex((
      "if",
      "unless",
      "for",
      "whenever"
    )) : w => text(fill: conditionalColor, weight: "bold", w)
    show kw2regex(
      ("value","update","stall","flush"),
    ) : w => text(fill: OliveGreen, w)
    show kw2regex(
      ("defsig", "new-trace")
    ) : w => text(fill: red, w)

    show regex("\"[^\"]*\"") : w => text(fill: stringColor, w)

    show regex("\(\*@ .* @\*\)") : w => {}

    it
  })

  #show raw.where(lang: "swim_toml") : style_lang((it, _number) => {
    show regex("#.*") : w => text(fill: commentGreen, w)
    show regex("\[[\w_.]*\]") : w => text(fill: builtinColor, w)
    show kw2regex(("git", "branch", "path")) : w => text(fill: keywordColor, w)
    show regex("\"[^\"]*\"") : w => text(fill: stringColor, w)

    it
  })

  #show raw.where(lang: "error") : style_lang((it, _number) => {
    show regex("@r[^@]*r@") : w => text(fill: errorRed, w.text.replace("@r", "").replace("r@", ""))
    show regex("@b[^@]*b@") : w => text(fill: errorBlue, w.text.replace("@b", "").replace("b@", ""))
    show regex("@o[^@]*o@") : w => text(fill: errorOrange, w.text.replace("@o", "").replace("o@", ""))

    show regex("┌|─|│|~|╭|╰") : w => text(fill: errorBlue, w);
    show regex("\^") : w => text(fill: errorRed, w);

    // #set par(spacing: 0pt)

    it
  })

  #show raw.where(lang: "swim_test_out") : style_lang((it, _number) => {
    show kw2regex(("FAIL", "FAILED")) : w => text(fill: errorRed, w)
    show kw2regex(("ok", )) : w => text(fill: okGreen, w)
    show regex("\[.*\]") : w => text(fill: gray.darken(50%), w)

    it
  })

  #show raw.where(lang: "sv_") : style_lang((it, number) => {
    show kw2regex((
      "logic",
    )) : set text(fill: keywordColor)

    show regex("\b\d+\b") : w => text(fill: literalColor, w)

    show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&", "\*", "\[", "\]", "\(", "\)", "\{", "\}")
    ) : w => text(fill: operatorColor, w)

    show regex("\.[A-z_0-9]+") : it => {
      text(fill: paramColor, it)
    }

    it
  })

  #show raw.where(lang: "python_") : style_lang((it, number) => {
    show kw2regex((
      "async",
      "def",
      "await",
      "import",
      "class",
      "return",
      )) : set text(fill: keywordColor)

    show kw2regex((
      "except",
      "try",
    )) : set text(fill: conditionalColor)

    show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&", "\*")
    ) : w => text(weight: "bold", fill: operatorColor, w)
    show regex("@.*") : w => emph(text(fill: attributeColor, w))
    show regex("\"[^\"]*\"") : w => text(fill: stringColor, w)

    show regex("#.*") : w => text(fill: commentGreen, w)

    it
  })

  #show raw.where(lang: "verilator") : style_lang((it, _number) => {
    show kw2regex((
      "async",
      "def",
      "await",
      )) : set text(fill: keywordColor)

    show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&", "\*")
    ) : w => text(weight: "bold", fill: operatorColor, w)
    show regex("@.*") : w => emph(text(fill: attributeColor, w))
    show regex("\"[^\"]*\"") : w => text(fill: stringColor, w)

    show kw2regex(("([A-Z_]+)",)) : set text(fill: constantColor)
    show regex("#\w+") : w => emph(text(fill: attributeColor, w))

    show regex("<.*>") : w => text(fill: stringColor, w)
    show regex("//.*") : w => text(fill: commentGreen, w)
    it
  })

  #doc
]

