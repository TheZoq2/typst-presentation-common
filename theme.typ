// University theme
//
// Originally contributed by Pol Dellaiera - https://github.com/drupol
// Adjusted by Frans Skarman

#import "./polylux/polylux.typ": *

#let uni-colors = state("uni-colors", (:))
#let uni-short-title = state("uni-short-title", none)
#let uni-short-author = state("uni-short-author", none)
#let uni-short-date = state("uni-short-date", none)
#let uni-progress-bar = state("uni-progress-bar", true)

#let university-theme(
  aspect-ratio: "16-9",
  short-title: none,
  short-author: none,
  short-date: none,
  content-color: white.darken(10%),
  color-b: black,
  background-color: black,
  progress-bar: true,
  body
) = {
  set page(
    paper: "presentation-" + aspect-ratio,
    margin: 2em,
    header: none,
    footer: none,
    fill: background-color
  )
  set text(size: 20pt, fill: content-color, font: "Nimbus Sans")
  show raw: set text(font: "Hasklug Nerd Font Mono")
  show footnote.entry: set text(size: .6em)

  uni-progress-bar.update(progress-bar)
  uni-colors.update((a: content-color, b: color-b, c: background-color))
  uni-short-title.update(short-title)
  uni-short-author.update(short-author)
  uni-short-date.update(short-date)

  body
}

#let title-slide(
  title: [],
  subtitle: none,
  authors: (),
  institution-name: "University",
  date: none,
  logo: none,
) = {
  let authors = if type(authors) ==  "array" { authors } else { (authors,) }

  let content = locate( loc => {
    let colors = uni-colors.at(loc)

    align(center + horizon, {
      block(
        inset: 0em,
        breakable: false,
        {
          text(size: 1.5em, fill: colors.a, strong(title))
          if subtitle != none {
            parbreak()
            text(size: 1.2em, fill: colors.a, subtitle)
          }
        }
      )
      set text(size: .8em)
      grid(
        columns: (1fr,) * calc.min(authors.len(), 4),
        column-gutter: 1em,
        row-gutter: 1em,
        ..authors.map(author => text(fill: white, author))
      )
      v(1em)
      if institution-name != none {
        parbreak()
        text(size: .9em, institution-name)
      }
      if date != none {
        parbreak()
        text(size: .8em, date)
      }

      if logo != none {
        align(right + bottom, logo)
      }
    })
  })

  themes.clean.logic.polylux-slide(content)
}


#let slide(
  title: none,
  columns: none,
  gutter: none,
  header: none,
  footer: none,
  new-section: none,
  no-margins: false,
  ..bodies
) = {
  let bodies = bodies.pos()
  let gutter = if gutter == none { 1em } else { gutter }
  let columns = if columns ==  none { (1fr,) * bodies.len() } else { columns }
  if columns.len() != bodies.len() {
    panic("number of columns must match number of content arguments")
  }

  let body = pad(x: 2em, y: .5em, grid(columns: columns, gutter: gutter, ..bodies))
  
  let progress-barline = locate( loc => {
    if uni-progress-bar.at(loc) {
      let cell = block.with( width: 100%, height: 100%, above: 0pt, below: 0pt, breakable: false )
      let colors = uni-colors.at(loc)

    } else { [] }
  })

  let header-text = {
    if header != none {
      header
    } else if title != none {
      if new-section != none {
        themes.clean.helpers.register-section(new-section)
      }
      locate( loc => {
        let colors = uni-colors.at(loc)
        block(fill: colors.c, inset: (x: .5em), 
          align(top + left, text(text(size: 1.2em, fill: colors.a, title))),
        )
      })
    } else { [] }
  }

  let header = {
    set align(top)
    grid(rows: (auto, auto), row-gutter: 3.5mm, progress-barline, header-text)
  }

  let footer = {
    set text(size: 10pt)
    set align(center + bottom)
    let cell(fill: none, it) = rect(
      width: 100%, height: 100%, inset: 1mm, outset: 0mm, fill: fill, stroke: none,
      align(horizon, text(fill: white, it))
    )
    if footer != none {
      footer
    } else {
      locate( loc => {
        let colors = uni-colors.at(loc)

        show: block.with(width: 100%, height: auto, fill: colors.b)
        grid(
          columns: (75%, 15%, 10%),
          rows: (1.5em, auto),
          cell(uni-short-title.display()),
          cell(uni-short-date.display()),
          cell(themes.clean.logic.logical-slide.display() + [~])
        )
      })
    }
  }


  set page(
    margin: if no-margins {0%} else {( top: 2em, bottom: 1em, x: 0em )},
    header: if no-margins {none} else {header},
    footer: if no-margins {none} else {footer},
    footer-descent: if no-margins {0%} else {0em},
    header-ascent: if no-margins {0%} else {.6em},
  )

  themes.clean.logic.polylux-slide(box(height: 100%, body))
}

#let focus-slide(background-color: none, background-img: none, body) = {
  set page(fill: background-color, margin: 1em) if background-color != none
  set page(
    background: {
      set image(fit: "stretch", width: 100%, height: 100%)
      background-img
    },
    margin: 1em,
  ) if background-img != none

  set text(fill: white, size: 1.5em)

  themes.clean.logic.polylux-slide(align(horizon, body))
}

#let matrix-slide(columns: none, rows: none, ..bodies) = {
  let bodies = bodies.pos()

  let columns = if type(columns) == "integer" {
    (1fr,) * columns
  } else if columns == none {
    (1fr,) * bodies.len()
  } else {
    columns
  }
  let num-cols = columns.len()

  let rows = if type(rows) == "integer" {
    (1fr,) * rows
  } else if rows == none {
    let quotient = calc.quo(bodies.len(), num-cols)
    let correction = if calc.rem(bodies.len(), num-cols) == 0 { 0 } else { 1 }
    (1fr,) * (quotient + correction)
  } else {
    rows
  }
  let num-rows = rows.len()

  if num-rows * num-cols < bodies.len() {
    panic("number of rows (" + str(num-rows) + ") * number of columns (" + str(num-cols) + ") must at least be number of content arguments (" + str(bodies.len()) + ")")
  }

  let cart-idx(i) = (calc.quo(i, num-cols), calc.rem(i, num-cols))
  let color-body(idx-body) = {
    let (idx, body) = idx-body
    let (row, col) = cart-idx(idx)
    let color = if calc.even(row + col) { white } else { silver }
    set align(center + horizon)
    rect(inset: .5em, width: 100%, height: 100%, fill: color, body)
  }

  let content = grid(
    columns: columns, rows: rows,
    gutter: 0pt,
    ..bodies.enumerate().map(color-body)
  )

  themes.clean.logic.polylux-slide(content)
}
