#import "./polylux/polylux.typ": *

#let subtle-content(content) = [
  #let color = gray.darken(25%)
  #show strong: set text(fill: color, weight: 100)
  #set text(fill: color)
  #content
]

// Stolen from polylux which doesn't expose it
// https://github.com/andreasKroepelin/polylux/blob/main/logic.typ#L19C1-L49C2
#let parse-subslide-indices(s) = {
  let parts = s.split(",").map(p => p.trim())
  let parse-part(part) = {
    let match-until = part.match(regex("^-([[:digit:]]+)\|?$"))
    let match-beginning = part.match(regex("^([[:digit:]]+)-\|?$"))
    let match-range = part.match(regex("^([[:digit:]]+)-([[:digit:]]+)\|?$"))
    let match-single = part.match(regex("^([[:digit:]]+)\|?$"))

    let has-pipe = part.ends-with("|")

    if match-until != none {
      let parsed = int(match-until.captures.first())
      // assert(parsed > 0, "parsed idx is non-positive")
      ( until: parsed, has-pipe: has-pipe )
    } else if match-beginning != none {
      let parsed = int(match-beginning.captures.first())
      // assert(parsed > 0, "parsed idx is non-positive")
      ( beginning: parsed, has-pipe: has-pipe )
    } else if match-range != none {
      let parsed-first = int(match-range.captures.first())
      let parsed-last = int(match-range.captures.last())
      // assert(parsed-first > 0, "parsed idx is non-positive")
      // assert(parsed-last > 0, "parsed idx is non-positive")
      ( beginning: parsed-first, until: parsed-last, has-pipe: has-pipe )
    } else if match-single != none {
      let parsed = int(match-single.captures.first())
      // assert(parsed > 0, "parsed idx is non-positive")
      (beginning: parsed, until: parsed, has-pipe: has-pipe )
    } else {
      panic("failed to parse visible slide idx: " + part)
    }
  }
  parts.map(parse-part)
}

#let hl-inner(duration, content, before: none, during: none, re-hidden: none, after: none) = {
  let indices = parse-subslide-indices(duration)

  let regions = ()

  // The inclusive end of the previous span
  let previous_end = 1;
  for i in range(0, indices.len()) {
    let span = indices.at(i)
    let begin = span.at("beginning", default: none)
    let until = span.at("until", default: none)
    // Special case the 0th span because there we support -X
    if i == 0 {
      if begin == none {
        regions.push((during(content), 1, until))
      } else {
        regions.push((before(content), 1, begin - 1))
        regions.push((during(content), begin, until))
      }
    } else {
      if begin == none {
        panic("Only the first region can start with - in span " + str(i) + "which is " + str(span))
      }
      regions.push((re-hidden(content), previous_end+1, begin - 1))
      regions.push((during(content), begin, until))
    }

    previous_end = until
  }

  for (wrapped-content, start, end) in regions {
    only(str(start) + "-" + str(end), wrapped-content)
  }

  let last_until = indices.last().at("until", default: none);
  let has-pipe = indices.last().at("has-pipe", default: none);

  if last_until != none and not has-pipe {
    only(str(last_until+1) + "-", after(content))
  }
}


#let hl(duration, content) = {
  hl-inner(
    duration,
    content,
    before: subtle-content,
    during: c => c,
    re-hidden: subtle-content,
    after: subtle-content
  )
}

#let reveal(duration, content) = {
  hl-inner(
    before: c => {},
    during: c => c,
    re-hidden: c => subtle-content(c),
    after: c => subtle-content(c),
    duration,
    content,
  )
}


