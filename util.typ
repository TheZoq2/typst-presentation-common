#import "@preview/acrostiche:0.5.1": *
#import "./typst-drafting/drafting.typ": *

#let inline-note_(..args) = {
  inline-note(..args)
  v(1fr)
}

#let ac(x) = {
  if type(x) == content {
    acr(x.text)
  } else {
    acr(x)
  }
}
#let acp(x) = {
  if type(x) == content {
    acrpl(x.text)
  } else {
    acrpl(x)
  }
}
#let ace(x) = {
  if type(x) == content {
    display-def(x.text)
  } else {
    display-def(x)
  }
}
#let acep(x) = {
  if type(x) == content {
    display-def(x.text, plural: true)
  } else {
    display-def(x, plural: true)
  }
}

#let todo(x) = margin-note({set text(size: 7pt); x})
#let todoi(x) = {
  block()
  line(length: 100%, stroke: orange.darken(20%))
  set text(fill: orange)
  line(length: 100%, stroke: orange.darken(20%))
  x
  block()
}
#let unsure(x) = margin-note(stroke: green, {set text(size: 7pt); x})
#let note(x) = inline-note_(text(fill: blue, x), stroke: blue)
#let info(x) = inline-note_(text(fill: gray, x), stroke: gray)

#let citep() = [[N]]

#let line_numbers = state("line_numbers", (:))

#let push_line_number(number, name) = {
  if number == none {
    panic("Pushed line number is none, not int")
  }
  line_numbers.update(current => {
    if name in current.keys() {
      panic("Duplicate line number name '" + name + "'")
    }
    current.insert(name, number)
    current
  })
}

#let lookup_line(name) = {
    let number = line_numbers.final().at(name, default: none)
    if number == none {
      panic(name + " not found in dict. Current dict: ", line_numbers.get())
    }
    return number
}

#let raw_line(name) = {
  context {
    [#lookup_line(name)]
  }
}

#let line_num(name) = {
  context {
    [Line~#lookup_line(name)]
  }
}

#let line_nums(start, end) = {
  context {
    [Lines~#lookup_line(start)-#lookup_line(end)]
  }
}
