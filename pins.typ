#import "@preview/pinit:0.1.3": *

#let pin_up(pin) = {
    box(height: 0.7em, [#box()#label(pin)])
}

#let pin_down(pin) = {
    box(height: 0em, [#box()#label(pin)])
}

#let pinned(content, pin_base) = {
  context {
    if query(selector(label(pin_base + "s")).before(inclusive: true, here())).len() == 0 {
      pin_up(pin_base + "s")
      content
      pin_down(pin_base + "e")
    } else {
      content
    }
  }
}

#let pinned_range(content, pin_base) = {
  context {
    if query(selector(label(pin_base + "s")).before(inclusive: true, here())).len() == 0 {
      box(height: 0.7em, [#box()#label(pin_base + "s")])
      content
      box(height: 0em, [#box()#label(pin_base + "e")])
    } else {
      content
    }
  }
}

#let bb_size(bounds) = {
    (width: bounds.right_x - bounds.left_x, height: bounds.bot_y - bounds.top_y)
}

#let highlight-block(
  pin,
  bound,
  style,
  line-side: left,
) = {
  let size = bb_size(bound)

  place(
    dx: bound.left_x,
    dy: bound.top_y,
    block(
      width: size.width,
      height: size.height,
      stroke: if line-side == right {
        (bottom: style.stroke, right: style.stroke)
      } else {
        (bottom: style.stroke, left: style.stroke)
      },
      outset: style.outset,
      radius: 0.4em
    )
  )
}

#let arrow(
  (from_x, from_y),
  from_dir,
  (to_x, to_y),
  style
) = {
  let from_x = from_x + if from_dir == left {-style.outset} else {style.outset}
  let to_x = to_x - style.content_outset
  let (foffset_x, foffset_y) = if from_dir == left {(-0.4em, 0em)} else {(0.4em, 0em)}
  let (toffset_x, toffset_y) = (-0.4em, 0em)
  place(
    dx: 0em,
    dy: 0em,
    path(
      stroke: (paint: style.flat_color, thickness: 0.1em),
      closed: false,
      (from_x, from_y),
      ((from_x + foffset_x, from_y), (-foffset_x, foffset_y)),
      ((from_x + foffset_x, to_y), (toffset_x, toffset_y)),
      // ((from_x + foffset_x, from_y + foffset_y), (-foffset_x, foffset_y)),
      // ((from_x + foffset_x, to_y), (foffset_x, foffset_y)),
      // (from_dir, (0em, 0em)),
      // (to_dir, (0em, 0em)),
      // ((to_x + to_dir_x, to_y + to_dir_y)),
      // ((to_x + toffset_x, to_y + toffset_y), (toffset_x, toffset_y)),
      (to_x, to_y)
    )
  )
}

#let point-at(
  pins,
  content,
  ypos: bottom,
  xoffset: 0em,
  yoffset: 2em,
  line-side: left,
  content-side: left
) = {
  let cmap = color.map.rainbow.map(it => it.lighten(90%).saturate(80%))
  let style = (
    cmap: cmap,
    flat_color: cmap.at(0),
    stroke: (paint: gradient.linear(..cmap), thickness: 0.1em),
    outset: 0.3em,
    content_outset: 0.3em,
  )
  context{

    let hpos = here().position()

    let bounding_boxes = pins.map((pin) => {
      let spos = query(selector(label(pin + "s"))).map(match => match.location().position()).at(0)
      let epos = query(selector(label(pin + "e"))).map(match => match.location().position()).at(0)
      // let spos = locate(selector(label()).before(here())).position()
      // let epos = locate(selector(label()).before(here())).position()

      let left_x = spos.x - hpos.x
      let top_y = spos.y - hpos.y

      let right_x = epos.x - hpos.x
      let bot_y = epos.y - hpos.y

      (left_x: left_x, right_x: right_x, top_y: top_y, bot_y : bot_y)
    })

    for (pin, bound) in pins.zip(bounding_boxes) {
        highlight-block(pin, bound, style, line-side: line-side)
    }

    let xstarts = bounding_boxes.map(it => it.left_x)
    let xends = bounding_boxes.map(it => it.right_x)
    let ystarts = bounding_boxes.map(it => it.bot_y)

    let rightmost_start = xstarts.sorted().at(0)
    let bottommost_start = ystarts.sorted().at(0)

    let explainer_x = if content-side == left {
      rightmost_start + xoffset
    } else {
      xends.sorted().rev().at(0) + xoffset
    }
    let explainer_y = bottommost_start + yoffset

    place(
      dx: explainer_x,
      dy: explainer_y,
      block(
        outset: style.content_outset,
        radius: 0.4em,
        fill: black,
        stroke: (paint: style.flat_color, thickness: 0.1em),
        content
      )
    )

    for bound in bounding_boxes {
      let from_x = if line-side == left {bound.left_x} else {bound.right_x}
      let from_y = bound.top_y + (bound.bot_y - bound.top_y) / 2
      let from_dir = if line-side == left {(-0.3em, 0em)} else {(0.3em, 0em)}
      arrow(
        (from_x, from_y),
        line-side,
        (explainer_x, explainer_y),
        style
      )
    }

  }

  context{
    let hpos = here().position()
  }
}
